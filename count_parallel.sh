#! /bin/bash
# usage: qsub count_parallel.sh
# See https://support.10xgenomics.com/single-cell-gene-expression/software/pipelines/latest/using/count
# for more details

#$ -cwd
#$ -l h_rt=72:00:00
#$ -N count_10x
#$ -l h_vmem=32g
#$ -l m_mem_free=32g
#$ -P xavier_lab
#$ -o logs/stdout
#$ -e logs/stderr

set -x

source /broad/software/scripts/useuse

use .cellranger-1.2.1
use .cellranger-2.0.0

dirs_list=dirs_list.txt

#hardcoded for now
species="mm10"

if [$species -eq "mm10"]
then
    ref=/broad/xavierlab_datadeposit/reference/10x_mm10/refdata-cellranger-mm10-1.2.0
else
    ref=/broad/xavierlab_datadeposit/reference/10x_hg19/refdata-cellranger-hg19-1.2.0
fi

sample_dir=`awk 'NR==n' n=$SGE_TASK_ID $dirs_list`

temp=${sample_dir##*/};
sample_name=${temp%%.*};

cellranger count --fastqs=$sample_dir --id=$sample_name --localcores=32 --localmem=32 --transcriptome=$ref > out_$sample_name.txt

