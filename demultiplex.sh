#! /bin/bash
# usage example (cluster): qsub demultiplex.sh raw/ samplesheet.csv
# usage example (local): sh demultiplex.sh raw/ samplesheet.csv
# See https://support.10xgenomics.com/single-cell-gene-expression/software/pipelines/latest/using/bcl2fastq-direct
# for more options

#$ -cwd
#$ -l h_rt=72:00:00
#$ -N demultiplex_10x
#$ -l h_vmem=24g
#$ -l m_mem_free=8g
#$ -P xavier_lab
#$ -o logs/stdout
#$ -e logs/stderr

set -x

source /broad/software/scripts/useuse

use .bcl2fastq2-2.17.1.14 
use .cellranger-1.2.1
use .cellranger-2.0.0

datadir=$1
samplesheet=$2

echo ${datadir}

cellranger mkfastq --run=$datadir --csv=$samplesheet

