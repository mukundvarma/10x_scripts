# 10x scripts for UGER cluster submission at the Broad

For count_serial, the first argument is the list of directories containing the fastq files for the different directories.

e.g. H37NYALXX/outs/fastq_path/H37NYALXX/mouse*

FOr count_parallel, this list of directories needs to be put into a text file prior to submitting the job.

e.g. ls H37NYALXX/outs/fastq_path/H37NYALXX/mouse* > dir_list.txt

The script file (count_parallel.sh) also needs to be edited to specify the correct number of jobs for the task array [ -t ] and the species to use [ mm10 or hg19]

The job can then be submitted using qsub count_parallel.sh

**Required reading:** https://support.10xgenomics.com/single-cell-gene-expression/software/pipelines/latest/what-is-cell-ranger