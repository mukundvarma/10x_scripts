#! /bin/bash

sample_dir=$1
species=$2

if [$species -eq"mm10"]
then
    ref=/broad/xavierlab_datadeposit/varma/reference/10x_mm10/refdata-cellranger-mm10-1.2.0
else
    ref=/broad/xavierlab_datadeposit/varma/reference/10x_hg19/refdata-cellranger-hg19-1.2.0
fi


for i in $sample_dir;
do
    samplename=${i##*/};
    cellranger count --fastqs=$i --id=$samplename --localcores=32 --localmem=32 --transcriptome=$ref > out_$samplename.txt
done

